﻿// Прокси вызова модулей других библиотек
//  
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

// Возвращает общий модуль ом_Значение
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Значение() Экспорт
	
	Возврат Модуль("ом_Значение");
	
КонецФункции // ом_Значение 

// Возвращает общий модуль ом_Коллекция
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Коллекция() Экспорт
	
	Возврат Модуль("ом_Коллекция");
	
КонецФункции // ом_Коллекция 

// Возвращает общий модуль ом_ТипИмя
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_ТипИмя() Экспорт
	
	Возврат Модуль("ом_ТипИмя");
	
КонецФункции // ом_ТипИмя 

// Возвращает общий модуль ом_Тип
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Тип() Экспорт
	
	Возврат Модуль("ом_Тип");
	
КонецФункции // ом_Тип 

// Возвращает общий модуль ом_Версия
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Версия() Экспорт
	
	Возврат Модуль("ом_Версия");
	
КонецФункции // ом_Версия 

// Возвращает общий модуль ом_Строка
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Строка() Экспорт
	
	Возврат Модуль("ом_Строка");
	
КонецФункции // ом_Строка 

// Возвращает общий модуль ом_Система
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_Система() Экспорт
	
	Возврат Модуль("ом_Система");
	
КонецФункции // ом_Система 

// Возвращает общий модуль ом_СистемаИнформация
//
// Параметры: 
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция ом_СистемаИнформация() Экспорт
	
	Возврат Модуль("ом_СистемаИнформация");
	
КонецФункции // ом_СистемаИнформация 

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// ВОзвращает общий модуль по имени
//
// Параметры: 
// 	Имя - Строка - Имя модуля
//
// Возвращаемое значение: 
// 	ОбщийМодуль
//
Функция Модуль(Имя)
	
	Возврат Вычислить(Имя);
	
КонецФункции // Модуль 

#КонецОбласти

// BSLLS:EmptyRegion-on
// BSLLS:NestedFunctionInParameters-on
 
﻿// Набор записей Adodb.Recordset
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Возвращает новый набор записей ADODB.Recordset
//
// Параметры: 
// 	Подключение - Adodb.Connection - Подключение к источнику данных
// 	КурсорТип - Число - Тип курсора, константы adOpen*, умолчание adOpenForwardOnly
// 	БлокировкаТип - Число - Тип блокировки записей, константы adLock*, умолчание adLockReadOnly
//
// Возвращаемое значение: 
// 	Adodb.Recordset
//
Функция Создать(КурсорТип = Null, БлокировкаТип = Null) Экспорт
	
	Результат = Новый COMОбъект("ADODB.Recordset");
	Результат.CursorType	= ?(КурсорТип = Null, кнр_AdodbКонстанта.adOpenForwardOnly(), КурсорТип);
	Результат.LockType		= ?(БлокировкаТип = Null, кнр_AdodbКонстанта.adLockReadOnly(), БлокировкаТип);
	
	Возврат Результат;
	
КонецФункции // Создать 

// Открывает набор записей внешнего источника. Тип курсора и тип блокировки задаются при создании курсора
//
// Параметры: 
// 	Подключение - Adodb.Connection - Подключение к поставщику данных
// 	Набор - Adodb.Recordset - Объявление набора записей
// 	Текст - Строка - Текст запроса
// 	Опции - Число - Опции выполнения, константы adCmd*, умолчание adCmdUnknown, 
//
Процедура Открыть(Подключение, Набор, Текст, Опции = Null) Экспорт
	
	Набор.Open(Текст, Подключение, , , ?(Опции = Null, кнр_AdodbКонстанта.adCmdUnknown(), Опции));
	
КонецПроцедуры // Открыть 

// Закрывает набор записей внешнего источника
//
// Параметры: 
// 	Набор - Oledb.Recordset - Набор записей
//
Процедура Закрыть(Набор) Экспорт
	
	Набор.Close();
	
КонецПроцедуры // Закрыть 

// Возвращает коллекцию полей набора, ключ имя поля, значение тип поля. Типы см. в 
// 	кнр_AdodbКонстанта, раздел типов
//
// Параметры: 
// 	Набор - Adodb.Recordset - Исследуемый набор
//
// Возвращаемое значение: 
// 	Соответствие
//
Функция ПоляКоллекция(Набор) Экспорт
	
	Результат	= Новый Соответствие();
	
	Поля		= Набор.Fields;
	Граница		= Поля.Count - 1;
	Для Индекс = 0 По Граница Цикл
		Поле	= Поля.Item(Индекс);
		Результат.Вставить(Поле.Name, Поле.Type);
	КонецЦикла;
	
	Возврат Результат;
	
КонецФункции // ПоляКоллекция 

// Возвращает таблицу значений по структуре полей набора
//
// Параметры: 
// 	Поля - Соответствие - Коллекция описания полей
// 	Имена - Соответствие - Маппинг имен полей на колонки таблица, ключ - имя поля
//
// Возвращаемое значение: 
// 	ТаблицаЗначений - Новая таблица по описаниям полей
//
Функция ПоляКоллекцияВТаблица(Поля, Имена = Null) Экспорт
	
	Колонки	= Новый Соответствие();
	Если Имена = Null Тогда
		Для Каждого Элемент Из Поля Цикл
			Имя	= Элемент.Ключ;
			Колонки.Вставить(Имя, Имя);
		КонецЦикла;
	Иначе
		Колонки	= Имена;
	КонецЕсли;
	
	
	
КонецФункции // ПоляКоллекцияВТаблица

//// Возвращает таблицу значений по набору записей Adodb
////
//// Параметры: 
//// 	Набор - Adodb.Recordset - Набор записей внешнего источника
//// 	Имена - Соответствие - Соответствие имен полей набора и результатирующей таблицы, ключ - имя в наборе
//// 	Таблица - ТаблицаЗначений - Дополняемая таблица
////
//// Возвращаемое значение: 
//// 	ТаблицаЗначений
////
//Функция ВТаблица(Набор, Имена, Таблица = Null) Экспорт
//	
//	СоответствиеТипов	= ?(ТипыСоответствие = Неопределено, ПолучитьСоответствиеТиповSQL1С(), ТипыСоответствие);
//	
//	// Запрос и заполнение результата
//	Набор	= НаборЗаписейСоздать();
//	НаборЗаписейОткрыть(Подключение, Набор, ЗапросТекст, кнр_AdodbКонстанта.adCmdText());
//	
//	ТипСтрока	= Новый ОписаниеТипов("Строка");
//	ТипЧисло	= Новый ОписаниеТипов("Число");
//	ТипДата		= Новый ОписаниеТипов("Дата");
//	ТипБулево	= Новый ОписаниеТипов("Булево");
//	ТипУИД		= Новый ОписаниеТипов("УникальныйИдентификатор");
//	
//	// Перебор полученного 
//	Пока НЕ Набор.EOF() Цикл
//		
//		РезультатСтрока	= Таблица.Добавить();
//		
//		Для каждого РезультатПоле Из РезультатСтруктура Цикл
//			
//			ПолеТип		= СоответствиеТипов.Получить(ИсточникСтруктура[РезультатПоле.Ключ]);
//			Значение	= Набор.Fields(РезультатПоле.Ключ).Value;
//			Если Значение = NULL Тогда
//				Продолжить;
//			КонецЕсли;
//			
//			Если ПолеТип = ТипДата И кнр_Модуль.ом_Значение().СтрокаЭто(Значение) Тогда
//				Значение	= Дата(СтрЗаменить(СтрЗаменить(СтрЗаменить(Значение, "-", ""), " ", ""), ":", ""));
//			ИначеЕсли ПолеТип = ТипБулево Тогда 
//				Значение	= (Значение = 1);
//			ИначеЕсли ПолеТип = ТипЧисло Тогда 
//				Значение	= Число(Значение);
//			ИначеЕсли ПолеТип = ТипУИД Тогда 
//				Значение	= Новый УникальныйИдентификатор(СтрЗаменить(СтрЗаменить(Значение, "{", ""), "}", ""));
//			Иначе
//				Значение	= Строка(Значение);
//			КонецЕсли; 
//			
//			РезультатСтрока[РезультатПоле.Значение]	= Значение;
//		КонецЦикла; 
//		
//		Набор.MoveNext();
//	КонецЦикла;
//	
//	Набор.Close();	
//	
//КонецФункции // ВТаблица 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

// Создает таблицу значений по набору данных
//
// Параметры: 
// 	Набор - Adodb.Recordset - Исходный набор источника данных
// 	Поля - Соответствие - Соответствие полей набора полям таблицы
//
// Возвращаемое значение: 
// 	ТаблицаЗначений
//
Функция НаборТаблицаСоздать(Набор, Поля)
	
	
	
КонецФункции // НаборТаблицаСоздать 

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
 
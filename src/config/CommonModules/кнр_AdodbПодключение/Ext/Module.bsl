﻿// Соединение с Adodb поставщиком данных
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Возвращает ADODB соединение с внешним источником. Возможно исключение при открытии соединения
//
// Параметры
// 	Провайдер - Строка - Имя провайдера (поставщика) данных, 'Provider' строки подключения
// 	Адрес - Строка - Адрес размещения данных, сервер, 'Data Source' строки подключения
// 	Пользователь - Строка - Имя пользователя, 'UID' строки подключения
// 	Пароль - Строка - Пароль, 'PWD' строки подключения
// 	Каталог - Строка - Имя базы данных, 'Initial Catalog' строки подключения
// 	ТаймаутСоединения - Число - Таймаут ожидания соединения
// 	ТаймаутКоманда - Число - Таймаут выполнения команды
//
// Возвращаемое значение:
//   ADODB.Connection
//
Функция Открыть(Провайдер, Адрес, Каталог, Пользователь, Пароль, ТаймаутСоединение = 15, ТаймаутКоманда = 15) Экспорт
	
	Если НЕ кнр_Модуль.ом_СистемаИнформация().ПлатформаWindowsЭто() Тогда
		ВызватьИсключение кнр_AdodbТексты.ПлатформаПоддержкаОтсутствует();
	КонецЕсли;
	
	ПодключениеСтрока	= "Provider=" + Провайдер + "; "
	+ "Persist Security Info=True;"
	+ "Data Source=" + Адрес + ";"
	+ "Initial Catalog=" + Каталог + ";"
	+ "User ID=" + Пользователь + ";"
	+ "Password=""" + Пароль + """";
	
	Результат	= Новый COMОбъект("ADODB.Connection");
	Результат.ConnectionString	= ПодключениеСтрока;
	Результат.ConnectionTimeOut	= ТаймаутСоединение;
	Результат.CommandTimeOut	= ТаймаутКоманда;
	
	Попытка
		Результат.Open();
	Исключение
		ВызватьИсключение;
	КонецПопытки;
	
	Возврат Результат;
	
КонецФункции // Открыть

// Закрывает ADODB соединение 
//
// Параметры
//	Подключение - Adodb.Connection - Подключение к ADODB
//
Процедура Закрыть(Подключение) Экспорт
	
	Подключение.Close();
	
КонецПроцедуры // Закрыть

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
 
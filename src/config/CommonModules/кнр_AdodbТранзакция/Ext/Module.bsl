﻿// Транзакции Adodb
//
// BSLLS:NestedFunctionInParameters-off - без лишних переменных выполнение быстрее
// BSLLS:EmptyRegion-off - пустые области разрешены

#Область ПрограммныйИнтерфейс

// Открывает транзакцию
//
// Параметры: 
// 	Подключение - Adodb.Connection - Подключение к MSSQL
//
// Возвращаемое значение: 
// 	Adodb.Transaction
//
Функция Начать(Подключение) Экспорт
	
	Возврат Подключение.BeginTransaction();
	
КонецФункции // Начать 

// Фиксирует транзакцию
//
// Параметры
// 	Транзакция - ComОбъект - ADODB.Command, активная транзакция
//
Процедура Зафиксировать(Транзакция) Экспорт
	
	Транзакция.Commit();
    
КонецПроцедуры // Зафиксировать 

// Отменяет транзакцию
//
// Параметры
// 	Транзакция - Число - активная транзакция
//
Процедура Отменить(Транзакция) Экспорт
	
	Транзакция.RollBack();
	
КонецПроцедуры // Отменить 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:NestedFunctionInParameters-on
// BSLLS:EmptyRegion-on
 